from mongo_client.config_db import collection

from passlib.context import CryptContext

from users.user_serializer import users_serializer


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

def get_password_hash(password):
    return pwd_context.hash(password)

def create_user(user):
    user['password'] = get_password_hash(user['password'])
    result = collection.insert_one(dict(user))
    assert result.acknowledged
    document = collection.find_one({"_id": result.inserted_id})
    if not document:
        raise print("id not found")
    user = users_serializer(collection.find({"_id": result.inserted_id}))
    return {"status": "Ok","data": user}
